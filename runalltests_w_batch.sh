#!/bin/bash
SRUN_COMMAND="" #"srun -J astaroth -p gpu --nodes=1 --gres=gpu:p100:1 --ntasks-per-node=1 --cpus-per-task=14 -t 02:59:00 --mem=48000"
USE_DOUBLE="ON"

module load cmake cuda
source ./sourceme.sh
cd build


cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} ./ac_run -t

