This sample was used to generate the CPU benchmarks for J. Pekkilä, Astaroth: A Library for Stencil Computations on Graphics Units. Master's thesis, Aalto University School of Science, Espoo, Finland, 2019.

Generating the benchmarks:
	- Clone Pencil Code from https://github.com/pencil-code/
	- Copy helical-MHDturb and runalltestscpu.sh to pencil-code/samples, overwriting if necessary
	- cd pencil-code/samples
	- ./runalltests.sh (WARNING: the script removes all directories in the current working directory matching helical-MHDturb_*)

The sample in helical-MHDturb is modified from a sample provided by Pencil Code (C) NORDITA. Pencil Code is licenced under GNU General Public Licence V2. See https://github.com/pencil-code/pencil-code/tree/master/license for more details.
