#!/bin/bash

rm -r helical-MHDturb_*
module restore system && module load intel && module load intelmpi

PRECISION="default"
ARRAY=(48 144 240 336 432 528)
for i in "${ARRAY[@]}"
do
    cp -rf helical-MHDturb helical-MHDturb_${i}_${PRECISION}
    cd helical-MHDturb_${i}_${PRECISION}
        pc_setupsrc && make clean
        sed -i s/"REAL_PRECISION = .*"/"REAL_PRECISION = ${PRECISION}"/ src/Makefile.local
        sed -i s/"nxgrid=[0-9]*"/"nxgrid=${i}"/ src/cparam.local
        sed -i s/"-O3"/"-O2"/ src/Makefile
        (make -j && pc_configtest && ./start.csh && ./run.csh) &> output.txt &
    cd -
done


PRECISION="double"
for i in "${ARRAY[@]}"
do
    cp -rf helical-MHDturb helical-MHDturb_${i}_${PRECISION}
    cd helical-MHDturb_${i}_${PRECISION}
        pc_setupsrc && make clean
        sed -i s/"REAL_PRECISION = .*"/"REAL_PRECISION = ${PRECISION}"/ src/Makefile.local
        sed -i s/"nxgrid=[0-9]*"/"nxgrid=${i}"/ src/cparam.local
        sed -i s/"-O3"/"-O2"/ src/Makefile
        (make -j && pc_configtest && ./start.csh && ./run.csh) &> output.txt &
    cd -
done
