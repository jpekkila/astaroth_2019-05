
======================================================================================================

# Astaroth - A Multi-GPU library for generic stencil computations

## The latest version of the library can be found at <https://bitbucket.org/jpekkila/astaroth/src/master/>.

======================================================================================================

---
*NOTE*

The following tests were used in J. Pekkil�, *Astaroth: A Library for Stencil Computations on Graphics Units*. Master's thesis, Aalto University School of Science, Espoo, Finland, 2019.

## The full list of built-in functions
* See file `src/core/kernels/rk3.cuh`

## Definition of the radial explosion initial values
* See function `gaussian_radial_explosion` in file `src/standalone/model/host_memory.cc`

## Pencil Code sample used for benchmarking
* See directory `pencil-code_test_sample`

## CPU arithmetic accuracy test
* See file `cpu_accuracy_test/errortest.c`

See the instructions below on how to run the GPU solver.

---

## System requirements

NVIDIA GPU with >= 3.0 compute capability. See https://en.wikipedia.org/wiki/CUDA#GPUs_supported.

## Dependencies
`apt install flex bison gcc cmake nvidia-cuda-dev`

## Setup 3rd party libraries for real-time visualization

1. `cd 3rdparty`
1. `./setup_dependencies.sh` Note: this may take some time.

## Building 

1. `source ./sourceme.sh`
1. `mkdir build`
1. `cd build`
1. `cmake -DDOUBLE_PRECISION=OFF -DBUILD_DEBUG=OFF -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF ..` 
1. `../scripts/compile_acc.sh && make -j`
1. `./ac_run <options>`

If you encounter issues, recheck that the 3rd party libraries were successfully built during the previous step.

### Available options

- `-s` simulation
- `-b` benchmark
- `-t` automated test 

By default, the program does a real-time visualization of the simulation domain. The camera and the initial conditions can be controller by `arrow keys`, `pgup`, `pgdown` and `spacebar`.

## Generating documentation

Run `doxygen doxyfile` in the base directory. The generated files can be found in `doc/doxygen`. The main page of the documentation will be at `dox/doxygen/astaroth_doc_html/index.html`.

## Formatting

If you have clang-format, you may run `scripts/fix_style.sh`. This script will recursively fix style of all the source files down from the current working directory. The script will ask for a confirmation before making any changes. 

## Coding style.

### In a nutshell
- Use [K&R indentation style](https://en.wikipedia.org/wiki/Indentation_style#K&R_style) and 4 space tabs. 
- Line width is 100 characters
- Start function names after a linebreak in source files. 
- [Be generous with `const` type qualifiers](https://isocpp.org/wiki/faq/const-correctness). 
- When in doubt, see [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html).

### Header example:
```cpp
// Licence notice and doxygen description here
#pragma once
#include "avoid_including_headers_here.h"

/** Doxygen comments */
void global_function(void);
```


### Source example:
```cpp
#include "parent_header.h"

#include <standard_library_headers.h>

#include "other_headers.h"
#include "more_headers.h"

typedef struct {
	int data;
} SomeStruct;

static inline int small_function(const SomeStruct& stuff) { return stuff.data; }

// Pass constant structs always by reference (&) and use const type qualifier.
// Modified structs are always passed as pointers (*), never as references.
// Constant parameters should be on the left-hand side, while non-consts go to the right.
static void
local_function(const SomeStruct& constant_struct, SomeStruct* modified_struct)
{
	modified_struct->data = constant_struct.data;
}

void
global_function(void)
{
	return;
}
```

