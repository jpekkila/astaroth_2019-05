#!/bin/bash
# author: pekkila
#SBATCH -J astaroth
#SBATCH -o astaroth_stdout.out
#SBATCH -e astaroth_stderr.out
#SBATCH -p gpu
#SBATCH --nodes=1
#SBATCH --gres=gpu:p100:1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=14
#SBATCH -t 09:00:00
#SBATCH --mem=48000
#SBATCH --mail-type=END
#SBATCH --mail-user=johannes.pekkila@aalto.fi

# note, this job requests a total of 28 cores and 1 GPGPU cards
# note, submit the job from taito-gpu.csc.fi
# commands to manage the batch script
#   submission command
#     sbatch [script-file]
#   status command
#     squeue -u pekkila
#   termination command
#     scancel [jobid]

# For more information
#   man sbatch
#   more examples in Taito GPU guide in
#   http://research.csc.fi/taito-gpu

#./runalltests_w_batch.sh
./runalltests_w_batch_testonly_p100.sh

# This script will print some usage statistics to the
# end of file: astaroth_stdout.out
# Use that to improve your resource request estimate
# on later jobs.
seff $SLURM_JOBID
