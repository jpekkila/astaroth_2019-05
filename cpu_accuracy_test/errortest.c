#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <string.h>

// Compile and run with
// gcc -std=c11 errortest.c -lm && ./a.out

static const float cp = 2;
static const float cv = 1;
static const float gamma = 2 / 1; 

float
get_inv_rho(const float lnrho)
{
    return 1.f / expf(lnrho);    
}

long double
get_inv_rho_ld(const float lnrho)
{
    return 1.l / expl(lnrho);  
}

float
get_inv_T(const float ss, const float lnrho)
{
    return 1.f / expf(gamma * ss / cp + (gamma - 1.f) * lnrho); // Assuming LNT0 = 0, LNRHO0 = 0
}

long double
get_inv_T_ld(const float ss, const float lnrho)
{
    return 1.l / expl(gamma * ss / cp + (gamma - 1.l) * lnrho); // Assuming LNT0 = 0, LNRHO0 = 0
}

long double
get_ulperror(const float candidate, const long double model)
{
    const int base = 2;
    const int p = 24;
    const int e = floorl(logl(fabsl(model))/ logl(2));
    const long double ulp = powl(base, e - (p-1));
    return fabsl(1.l - candidate/model) / ulp;
}


static void
print_error(const float ss, const float lnrho)
{
	const float inv_rho  = get_inv_rho(lnrho);
	const float inv_T    = get_inv_T(ss, lnrho);
	const float inv_rhoT = inv_rho * inv_T;

	const long double inv_rho_ld  = get_inv_rho_ld(lnrho);
	const long double inv_T_ld    = get_inv_T_ld(ss, lnrho);
	const long double inv_rhoT_ld = inv_rho_ld * inv_T_ld;

	const long double err_inv_rho  = get_ulperror(inv_rho, inv_rho_ld);
	const long double err_inv_T    = get_ulperror(inv_T, inv_T_ld);
	const long double err_inv_rhoT = get_ulperror(inv_rhoT, inv_rhoT_ld);

	char buffer[4096];
	sprintf(buffer, "%-16g, %-16g, %-16Lg, %-16Lg, %-16Lg", ss, lnrho, err_inv_rho, err_inv_T, err_inv_rhoT);
	printf("%s\n", buffer);
}

int
main(void)
{
    const float a = 1.23456789123456789;

    for (int i = 1; i <= 1e9; i*=10) {
        const float a = 1.0 / i;
        printf("a = %g\n", a);

        const float b = expf(a) * expf(a);
        const long double c = expl(a) * expl(a);

        printf("Relative error %Lg\n", fabsl(1.0l - b/c) / FLT_EPSILON);
        printf("Absolute error %Lg\n", fabsl(b - c));
        printf("\n");
    }


    // lnT = (gamma - 1) * lnrho 
    //const Scalar inv_rho = Scalar(1.) / exp(value(lnrho));
   //const Scalar inv_pT = Scalar(1.) / (exp(value(lnrho)) + exp(lnT(ss, lnrho)));

    char buffer[4096];
    sprintf(buffer, "%-16s, %-16s, %-16s, %-16s, %-16s", "ss", "lnrho", "err inv_rho ulps", "err inv_T ulps", "err inv_rhoT ulps");
    printf("%s\n", buffer);

    for (float ss = 1e-6; ss <= 1e6; ss *= 10) {
        for (float lnrho = 1e-6; lnrho <= 1e6; lnrho *= 10) {
		print_error(ss, lnrho);
        }
    }

    for (float ss = -1e6; ss < -1e-6; ss /= 10) {
        for (float lnrho = -1e6; lnrho < -1e-6; lnrho /= 10) {
		print_error(ss, lnrho);
        }
    }

    {
    const float ss = 1e-6;
    const float lnrho = 0e-6;
		print_error(ss, lnrho);
    }
    {
    const float ss = 0e-6;
    const float lnrho = 1e-6;
		print_error(ss, lnrho);
    }
    {
    const float ss = 1;
    const float lnrho = 0e-6;
		print_error(ss, lnrho);
    }
    {
    const float ss = 0e-6;
    const float lnrho = 1;
		print_error(ss, lnrho);
    }    
    return EXIT_SUCCESS;
}




































