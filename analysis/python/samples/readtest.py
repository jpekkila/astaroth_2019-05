'''
    Copyright (C) 2014-2019, Johannes Pekkilae, Miikka Vaeisalae.

    This file is part of Astaroth.

    Astaroth is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Astaroth is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Astaroth.  If not, see <http://www.gnu.org/licenses/>.
'''
import astar.data as ad
import astar.visual as vis
import pylab as plt 
import numpy as np 
import sys

##mesh = ad.read.Mesh(500, fdir="/tiara/home/mvaisala/astaroth-code/astaroth_2.0/build/")
##
##print(np.shape(mesh.uu))
##print(np.shape(mesh.lnrho))
##
##uu_tot = np.sqrt(mesh.uu[0]**2.0 + mesh.uu[1]**2.0 + mesh.uu[2]**2.0)
##vis.slices.plot_3(mesh, uu_tot, title = r'$|u|$', bitmap = True, fname = 'uutot')
##
##vis.slices.plot_3(mesh, mesh.lnrho, title = r'$\ln \rho$', bitmap = True, fname = 'lnrho')
##
##print(mesh.minfo.contents)



if 0:
    mesh = ad.read.Mesh(1, fdir="/tiara/home/mvaisala/astaroth-code/astaroth_2.0/build/")
    print(mesh.lnrho.shape)
    
    print( mesh.lnrho[1, 50, 100], 0.0)
    print( mesh.lnrho[197, 50, 100], 0.0)
    print( mesh.lnrho[100, 50, 1], 0.0)
    print( mesh.lnrho[100, 50, 197], 0.0)
    print( mesh.lnrho[100, 1, 100], "periodic")
    print( mesh.lnrho[100, 101, 00], "periodic")

    angle = 0.78
    UUXX = -0.25 * np.cos(angle)
    zorig = 4.85965
    zz = [0.0490874*1.0 - zorig,  0.0490874*100.0 - zorig, 0.0490874*197.0 - zorig]
    print (zz) 
    zz = np.array(zz)
    UUZZ = - 0.25*np.sin(angle)*np.tanh(zz/0.2)
    #plt.plot(np.linspace(-5.0, 5.0, num=100),- (0.25*np.sin(angle))*np.tanh(np.linspace(-5.0, 5.0, num=100)/0.2)) 
    #plt.show()
    print("---- UUX")
    print( mesh.uu[0][1, 50, 100], 0.0)
    print( mesh.uu[0][197, 50, 100], UUXX)
    print( mesh.uu[0][100, 50, 1], UUXX)
    print( mesh.uu[0][100, 50, 197], UUXX)
    print( mesh.uu[0][100, 1, 100], "periodic")
    print( mesh.uu[0][100, 101, 00], "periodic")
    print("---- UUY")
    print( mesh.uu[1][1, 50, 100], 0.0)
    print( mesh.uu[1][197, 50, 100], 0.0)
    print( mesh.uu[1][100, 50, 1], 0.0)
    print( mesh.uu[1][100, 50, 197], 0.0)
    print( mesh.uu[1][100, 1, 100], "periodic")
    print( mesh.uu[1][100, 101, 00], "periodic")
    print("---- UUZ")
    print( mesh.uu[2][1, 50, 100], 0.0)
    print( mesh.uu[2][197, 50, 100], UUZZ[1])
    print( mesh.uu[2][100, 50, 1],   UUZZ[0])
    print( mesh.uu[2][100, 50, 197], UUZZ[2])
    print( mesh.uu[2][100, 1, 100], "periodic")
    print( mesh.uu[2][100, 101, 00], "periodic")

if sys.argv[1] == 'xline':
    mesh = ad.read.Mesh(0, fdir="/tiara/ara/data/mvaisala/asth_testbed_double/")
    plt.figure()
    plt.plot(mesh.uu[0][100, 50, :] , label="z")
    plt.plot(mesh.uu[0][100, :, 100], label="x")
    plt.plot(mesh.uu[0][:, 50, 100] , label="y")
    plt.legend()

    plt.figure()
    plt.plot(mesh.uu[0][197, 50, :] , label="z edge")

    plt.figure()
    plt.plot(mesh.uu[1][100, 50, :] , label="z")
    plt.plot(mesh.uu[1][100, :, 100], label="x")
    plt.plot(mesh.uu[1][:, 50, 100] , label="y")
    plt.legend()

    plt.figure()
    plt.plot(mesh.uu[2][100, 50, :] , label="z")
    plt.plot(mesh.uu[2][100, :, 100], label="x")
    plt.plot(mesh.uu[2][:, 50, 100] , label="y")
    plt.legend()
    plt.show()

if sys.argv[1] == 'sl':
    for i in range(0, 20002):
        #mesh = ad.read.Mesh(i, fdir="/tiara/home/mvaisala/astaroth-code/astaroth_2.0/build/")
        mesh = ad.read.Mesh(i, fdir="/tiara/ara/data/mvaisala/asth_testbed_double/")
        if mesh.ok:
            uu_tot = np.sqrt(mesh.uu[0]**2.0 + mesh.uu[1]**2.0 + mesh.uu[2]**2.0)

            if sys.argv[2] == 'lim':
                vis.slices.plot_3(mesh, mesh.lnrho, title = r'$\ln \rho$', bitmap = True, fname = 'lnrho', colrange=[-0.0, 0.1])
                vis.slices.plot_3(mesh, mesh.uu[0], title = r'$u_x$', bitmap = True, fname = 'uux', colrange=[-0.15, 0.15])
                vis.slices.plot_3(mesh, mesh.uu[1], title = r'$u_y$', bitmap = True, fname = 'uuy', colrange=[-1.0e-17, 1.0e-17])
                vis.slices.plot_3(mesh, mesh.uu[2], title = r'$u_z$', bitmap = True, fname = 'uuz', colrange=[-0.15, 0.15])
                vis.slices.plot_3(mesh, np.exp(mesh.lnrho), title = r'$N_\mathrm{col}$', bitmap = True, fname = 'colden', slicetype = 'sum', colrange=[0.0, 100.0])
                vis.slices.plot_3(mesh, uu_tot, title = r'$|u|$', bitmap = True, fname = 'uutot', colrange=[0.0, 0.2])
            else: 
                vis.slices.plot_3(mesh, mesh.lnrho, title = r'$\ln \rho$', bitmap = True, fname = 'lnrho')
                vis.slices.plot_3(mesh, mesh.uu[0], title = r'$u_x$', bitmap = True, fname = 'uux')
                vis.slices.plot_3(mesh, mesh.uu[1], title = r'$u_y$', bitmap = True, fname = 'uuy')
                vis.slices.plot_3(mesh, mesh.uu[2], title = r'$u_z$', bitmap = True, fname = 'uuz')
                vis.slices.plot_3(mesh, np.exp(mesh.lnrho), title = r'$N_\mathrm{col}$', bitmap = True, fname = 'colden', slicetype = 'sum')
                vis.slices.plot_3(mesh, uu_tot, title = r'$|u|$', bitmap = True, fname = 'uutot')
    
    

if sys.argv[1] == 'ts':
   ts = ad.read.TimeSeries(fdir="/tiara/ara/data/mvaisala/asth_testbed_double/")

   end_rm = -1 #-35#-40

   plt.figure()
   xaxis  = 't_step'
   yaxis1 = 'lnrho_rms'
   yaxis2 = 'lnrho_min'
   yaxis3 = 'lnrho_max'
   plt.plot(ts.var[xaxis][:end_rm], np.exp(ts.var[yaxis1][:end_rm]), label=yaxis1)
   plt.plot(ts.var[xaxis][:end_rm], np.exp(ts.var[yaxis2][:end_rm]), label=yaxis2)
   plt.plot(ts.var[xaxis][:end_rm], np.exp(ts.var[yaxis3][:end_rm]), label=yaxis3)
   plt.xlabel(xaxis)
   plt.legend()
  
   plt.figure()
   xaxis = 't_step'
   yaxis1 = 'uutot_rms'
   yaxis2 = 'uutot_min'
   yaxis3 = 'uutot_max'
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis1][:end_rm], label=yaxis1)
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis2][:end_rm], label=yaxis2)
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis3][:end_rm], label=yaxis3)
   plt.xlabel(xaxis)
   plt.legend()

   plt.figure()
   xaxis = 't_step'
   yaxis1 = 'uux_rms'
   yaxis2 = 'uux_min'
   yaxis3 = 'uux_max'
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis1][:end_rm], label=yaxis1)
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis2][:end_rm], label=yaxis2)
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis3][:end_rm], label=yaxis3)
   plt.xlabel(xaxis)
   plt.legend()
  
   plt.figure()
   xaxis = 't_step'
   yaxis1 = 'uuy_rms'
   yaxis2 = 'uuy_min'
   yaxis3 = 'uuy_max'
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis1][:end_rm], label=yaxis1)
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis2][:end_rm], label=yaxis2)
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis3][:end_rm], label=yaxis3)
   plt.xlabel(xaxis)
   plt.legend()
  
   plt.figure()
   xaxis = 't_step'
   yaxis1 = 'uuz_rms'
   yaxis2 = 'uuz_min'
   yaxis3 = 'uuz_max'
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis1][:end_rm], label=yaxis1)
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis2][:end_rm], label=yaxis2)
   plt.plot(ts.var[xaxis][:end_rm], ts.var[yaxis3][:end_rm], label=yaxis3)
   plt.xlabel(xaxis)
   plt.legend()
  
  
   plt.show()


