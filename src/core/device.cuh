/*
    Copyright (C) 2014-2018, Johannes Pekkilae, Miikka Vaeisalae.

    This file is part of Astaroth.

    Astaroth is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Astaroth is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Astaroth.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * \brief Brief info.
 *
 * Detailed info.
 *
 */
#pragma once
#include "astaroth.h"
#include "kernels/device_globals.cuh"

class Device {
  public:
    Device(const int& device_id, const AcMeshInfo& mesh_info);
    ~Device(void);

    AcReal reduce_scal(const StreamType stream_id,
                       const ReductionType rtype,
                       const VertexBufferHandle handle);
    AcReal reduce_vec(const StreamType stream_id,
                      const ReductionType rtype,
                      const VertexBufferHandle vertex_buffer_a,
                      const VertexBufferHandle vertex_buffer_b,
                      const VertexBufferHandle vertex_buffer_c);

    AcResult boundcond_step(const StreamType stream_id);

    AcResult rk3_step(const StreamType stream_id, const int step_number, const AcReal dt);
    AcResult rk3_step_inner(const StreamType stream_id, const int step_number, const AcReal dt);
    AcResult rk3_step_outer(const StreamType stream_id, const int step_number, const AcReal dt);

    /** Synchronizes all streams running on the device */
    AcResult synchronize(const StreamType stream_id);

  private:
    const int m_device_id;
    /* Concurrency */
    cudaStream_t m_streams[NUM_STREAM_TYPES];

    AcMeshInfo m_mesh_info; // Should be removed once multi-GPU reductions work

    /* Reduction */
    AcReal* m_reduce_scratchpad;
    AcReal* m_reduce_result;
};
