/*
    Copyright (C) 2014-2018, Johannes Pekkilae, Miikka Vaeisalae.

    This file is part of Astaroth.

    Astaroth is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Astaroth is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Astaroth.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * \brief Brief info.
 *
 * Detailed info.
 *
 */
#include "device.cuh"

//#include "kernels/boundconds.cuh"
#include "kernels/reduce.cuh"
#include "kernels/rk3.cuh"

__constant__ AcMeshInfo d_mesh_info;

static void
print_gpu_config(const int device_id)
{
    cudaDeviceProp props;
    cudaGetDeviceProperties(&props, device_id);
    printf("--------------------------------------------------\n");
    printf("Device Number: %d\n", device_id);
    const size_t bus_id_max_len = 128;
    char bus_id[bus_id_max_len];
    cudaDeviceGetPCIBusId(bus_id, bus_id_max_len, device_id);
    printf("  PCI bus ID: %s\n", bus_id);
    printf("    Device name: %s\n", props.name);
    printf("    Compute capability: %d.%d\n", props.major, props.minor);

    // Compute
    printf("  Compute\n");
    printf("    Clock rate (GHz): %g\n", props.clockRate / 1e6); // KHz -> GHz
    printf("    Stream processors: %d\n", props.multiProcessorCount);
    printf("    SP to DP flops performance ratio: %d:1\n", props.singleToDoublePrecisionPerfRatio);
    printf("    Compute mode: %d\n", (int)props.computeMode); // https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__TYPES.html#group__CUDART__TYPES_1g7eb25f5413a962faad0956d92bae10d0
    // Memory
    printf("  Global memory\n");
    printf("    Memory Clock Rate (MHz): %d\n", props.memoryClockRate / (1000));
    printf("    Memory Bus Width (bits): %d\n", props.memoryBusWidth);
    printf("    Peak Memory Bandwidth (GiB/s): %f\n",
           2 * (props.memoryClockRate * 1e3) * props.memoryBusWidth /
               (8. * 1024. * 1024. * 1024.));
    printf("    ECC enabled: %d\n", props.ECCEnabled);
    // Memory usage
    size_t free_bytes, total_bytes;
    ERRCHK_CUDA(cudaMemGetInfo(&free_bytes, &total_bytes));
    const size_t used_bytes = total_bytes - free_bytes;
    printf("    Total global mem: %.2f GiB\n",
           props.totalGlobalMem / (1024.0 * 1024 * 1024));
    printf("    Gmem used (GiB): %.2f\n", used_bytes / (1024.0 * 1024 * 1024));
    printf("    Gmem memory free (GiB): %.2f\n",
           free_bytes / (1024.0 * 1024 * 1024));
    printf("    Gmem memory total (GiB): %.2f\n",
           total_bytes / (1024.0 * 1024 * 1024));
    printf("  Caches\n");
    printf("    Local L1 cache supported: %d\n", props.localL1CacheSupported);
    printf("    Global L1 cache supported: %d\n", props.globalL1CacheSupported);
    printf("    L2 size: %d KiB\n", props.l2CacheSize / (1024));
    printf("    Total const mem: %ld KiB\n", props.totalConstMem / (1024));
    printf("    Shared mem per block: %ld KiB\n",
           props.sharedMemPerBlock / (1024));
    printf("  Other\n");
    printf("    Warp size: %d\n", props.warpSize);
    // printf("    Single to double perf. ratio: %dx\n",
    // props.singleToDoublePrecisionPerfRatio); //Not supported with older CUDA
    // versions
    printf("    Stream priorities supported: %d\n",
           props.streamPrioritiesSupported);
    printf("--------------------------------------------------\n");
}

static AcResult
load_dconsts(const AcMeshInfo& mesh_info, const cudaStream_t stream)
{
    ERRCHK_CUDA(cudaMemcpyToSymbolAsync(d_mesh_info, &mesh_info,
                                        sizeof(mesh_info), 0,
                                        cudaMemcpyHostToDevice, stream));
    return AC_SUCCESS;
}

static __global__ void dummy_kernel(void) {}

Device::Device(const int& device_id, const AcMeshInfo& mesh_info)
    : m_device_id(device_id)
{
    cudaSetDevice(m_device_id);
    cudaDeviceReset(); // Make sure that we're on a clean device
    print_gpu_config(m_device_id);

    printf("Trying to run a dummy kernel. If this fails, make sure that your\n"
            "device supports the CUDA architecture you are compiling for.\n"
            "Running dummy kernel... "); fflush(stdout);
    dummy_kernel<<<1, 1>>>();
    ERRCHK_CUDA_KERNEL_ALWAYS();
    printf("Success!\n");

    // Concurrency (flags: cudaStreamDefault, cudaStreamNonBlocking)
    int low, high;
    cudaDeviceGetStreamPriorityRange(&low, &high);
    for (int i = 0; i < NUM_STREAM_TYPES; ++i)
        ERRCHK_CUDA(cudaStreamCreateWithPriority(&m_streams[i], cudaStreamDefault, high + i));

    // Device constants
    m_mesh_info = mesh_info;
    load_dconsts(m_mesh_info, m_streams[STREAM_PRIMARY]);

    // Reduction
    const size_t scratchpad_size_bytes = AC_VTXBUF_COMPDOMAIN_SIZE_BYTES(
        mesh_info);
    ERRCHK_CUDA(cudaMalloc(&m_reduce_scratchpad, scratchpad_size_bytes));
    ERRCHK_CUDA(cudaMalloc(&m_reduce_result, sizeof(AcReal)));
}

Device::~Device(void)
{
    cudaSetDevice(m_device_id);

    // Mesh
    printf("Destructing\n");

    // Reduction
    ERRCHK_CUDA(cudaFree(m_reduce_scratchpad));
    ERRCHK_CUDA(cudaFree(m_reduce_result));

    // Concurrency
    for (int i = 0; i < NUM_STREAM_TYPES; ++i)
        ERRCHK_CUDA(cudaStreamDestroy(m_streams[i]));
    //ERRCHK_CUDA(cudaStreamDestroy(m_default_stream));
}

AcReal
Device::reduce_scal(const StreamType stream_id,
                    const ReductionType rtype,
                    const VertexBufferHandle handle)
{
    cudaSetDevice(m_device_id);

    return _reduce_scal(m_streams[stream_id],
                        rtype, m_mesh_info.int_params[AC_nx],
                        m_mesh_info.int_params[AC_ny],
                        m_mesh_info.int_params[AC_nz], d_buffer.in[handle],
                        m_reduce_scratchpad, m_reduce_result);
}

AcReal
Device::reduce_vec(const StreamType stream_id,
                   const ReductionType rtype,
                   const VertexBufferHandle vertex_buffer_a,
                   const VertexBufferHandle vertex_buffer_b,
                   const VertexBufferHandle vertex_buffer_c)
{
    cudaSetDevice(m_device_id);

    return _reduce_vec(m_streams[stream_id],
        rtype, m_mesh_info.int_params[AC_nx], m_mesh_info.int_params[AC_ny],
        m_mesh_info.int_params[AC_nz], d_buffer.in[vertex_buffer_a],
        d_buffer.in[vertex_buffer_b], d_buffer.in[vertex_buffer_c],
        m_reduce_scratchpad, m_reduce_result);
}

AcResult
Device::boundcond_step(const StreamType stream_id)
{
    cudaSetDevice(m_device_id);

    ERROR("Multi-GPU boundcond step not yet implemented!\n");
    (void) stream_id; // Unused
    /*
    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i)
        _boundcond_step(m_streams[stream_id], m_mesh_info, d_buffer.in[VertexBufferHandle(i)]);
    */
    return AC_SUCCESS;
}

#if RK3_OPTIMIZE == 1
AcResult
Device::rk3_step(const StreamType stream_id,
                 const int step_number, const AcReal dt)
{
    cudaSetDevice(m_device_id);

    const int3 start = (int3){m_mesh_info.int_params[AC_nx_min],
                              m_mesh_info.int_params[AC_ny_min],
                              m_mesh_info.int_params[AC_nz_min]};
    const int3 end   = (int3){m_mesh_info.int_params[AC_nx_max],
                              m_mesh_info.int_params[AC_ny_max],
                              m_mesh_info.int_params[AC_nz_max]};

    dim3 best_dims(0, 0, 0);
    float best_time = INFINITY;

    for (int z = 1; z <= MAX_TB_DIM; ++z) {
    for (int y = 1; y <= MAX_TB_DIM; ++y) {
    for (int x = WARP_SIZE; x <= MAX_TB_DIM; x+=WARP_SIZE) {

        if (x > end.x - start.x ||
            y > end.y - start.y ||
            z > end.z - start.z)
            break;
        if (x*y*z > MAX_THREADS_PER_BLOCK)
            break;

        if (x*y*z*REGISTERS_PER_THREAD > MAX_REGISTERS_PER_BLOCK)
            break;

        if (((x*y*z) % WARP_SIZE) != 0)
            continue;

    const dim3 tpb(x, y, z);

    // Warmup
    for (int step_number = 0; step_number < 3; ++step_number)
        rk3_step_async(m_streams[stream_id], tpb, start, end, step_number, FLT_EPSILON, m_mesh_info, &d_buffer);

    cudaDeviceSynchronize();
    if (cudaGetLastError() != cudaSuccess) // resets the error if any
        continue;

    printf("(%d, %d, %d)\n", x, y, z);

    cudaEvent_t tstart, tstop;
    cudaEventCreate(&tstart);
    cudaEventCreate(&tstop);

    cudaEventRecord(tstart); // ---------------------------------------- Timing start

    for (int i = 0; i < NUM_ITERATIONS; ++i)
        rk3_step_async(m_streams[stream_id], tpb, start, end, 2, FLT_EPSILON, m_mesh_info, &d_buffer);

    cudaEventRecord(tstop); // ---------------------------------------- Timing end
    cudaEventSynchronize(tstop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, tstart, tstop);

    ERRCHK_CUDA_KERNEL_ALWAYS();
    if (milliseconds < best_time) {
        best_time = milliseconds;
        best_dims = tpb;
    }
    }}}

    printf("Best dims (%d, %d, %d) %f ms\n", best_dims.x, best_dims.y, best_dims.z, double(best_time) / NUM_ITERATIONS);
    FILE* fp = fopen("../config/rk3_tbdims.cuh", "w");
    ERRCHK(fp);
    fprintf(fp, "%d, %d, %d\n", best_dims.x, best_dims.y, best_dims.z);
    fclose(fp);


    exit(0);
    return AC_SUCCESS;
}
#else // The default run
AcResult
Device::rk3_step(const StreamType stream_id,
                 const int step_number, const AcReal dt)
{
    cudaSetDevice(m_device_id);

    const int3 start = (int3){m_mesh_info.int_params[AC_nx_min],
                              m_mesh_info.int_params[AC_ny_min],
                              m_mesh_info.int_params[AC_nz_min]};
    const int3 end   = (int3){m_mesh_info.int_params[AC_nx_max],
                              m_mesh_info.int_params[AC_ny_max],
                              m_mesh_info.int_params[AC_nz_max]};

    static dim3 tpb(0, 0, 0);
    if (tpb.x == 0) {

        FILE* fp = fopen("../config/rk3_tbdims.cuh", "r");

        if (fp) { // Read threads per block from a file
            const int retval = fscanf(fp, "%d, %d, %d\n", &tpb.x, &tpb.y, &tpb.z);
            fclose(fp);
        }else { // Else use the default dims
            WARNING("rk3_tbdims.cuh not generated. Run it by passing -DRK3_OPTIMIZE to cmake. Using default threadblock dims.");
            tpb.x = 32; tpb.y = 1; tpb.z = 4;
        }
    }

    rk3_step_async(m_streams[stream_id], tpb, start, end, step_number, dt, m_mesh_info, &d_buffer);
    return AC_SUCCESS;
}
#endif

/*
AcResult
Device::rk3_step_inner(const StreamType stream_id,
                       const int step_number, const AcReal dt)
{
    cudaSetDevice(m_device_id);
    const int3 start = (int3){m_mesh_info.int_params[AC_nx_min] + STENCIL_ORDER/2,
                              m_mesh_info.int_params[AC_ny_min] + STENCIL_ORDER/2,
                              m_mesh_info.int_params[AC_nz_min] + STENCIL_ORDER/2};
    const int3 end   = (int3){m_mesh_info.int_params[AC_nx_max] - STENCIL_ORDER/2,
                              m_mesh_info.int_params[AC_ny_max] - STENCIL_ORDER/2,
                              m_mesh_info.int_params[AC_nz_max] - STENCIL_ORDER/2};

    rk3_step_async(m_streams[stream_id], start, end, step_number, dt, m_mesh_info, &d_buffer);
    return AC_SUCCESS;
}

AcResult
Device::rk3_step_outer(const StreamType stream_id,
                       const int step_number, const AcReal dt)
{
    cudaSetDevice(m_device_id);

    const int nx_min = m_mesh_info.int_params[AC_nx_min];
    const int ny_min = m_mesh_info.int_params[AC_ny_min];
    const int nz_min = m_mesh_info.int_params[AC_nz_min];

    const int nx_max = m_mesh_info.int_params[AC_nx_max];
    const int ny_max = m_mesh_info.int_params[AC_ny_max];
    const int nz_max = m_mesh_info.int_params[AC_nz_max];
    const int bound = STENCIL_ORDER/2;

    {
        const int3 start = (int3){nx_min, ny_min, nz_min};
        const int3 end   = (int3){nx_max, ny_max, nz_min + bound};
        rk3_step_async(m_streams[stream_id], start, end, step_number, dt, m_mesh_info, &d_buffer);
    }
    {
        const int3 start = (int3){nx_min, ny_min, nz_max - bound};
        const int3 end   = (int3){nx_max, ny_max, nz_max};
        rk3_step_async(m_streams[stream_id], start, end, step_number, dt, m_mesh_info, &d_buffer);
    }

    {
        const int3 start = (int3){nx_min, ny_min, nz_min + bound};
        const int3 end   = (int3){nx_min + bound, ny_max, nz_max - bound};
        rk3_step_async(m_streams[stream_id], start, end, step_number, dt, m_mesh_info, &d_buffer);
    }
    {
        const int3 start = (int3){nx_max - bound, ny_min, nz_min + bound};
        const int3 end   = (int3){nx_max, ny_max, nz_max - bound};
        rk3_step_async(m_streams[stream_id], start, end, step_number, dt, m_mesh_info, &d_buffer);
    }
    {
        const int3 start = (int3){nx_min + bound, ny_min, nz_min + bound};
        const int3 end   = (int3){nx_max - bound, ny_min + bound, nz_max - bound};
        rk3_step_async(m_streams[stream_id], start, end, step_number, dt, m_mesh_info, &d_buffer);
    }
    {
        const int3 start = (int3){nx_min + bound, ny_max - bound, nz_min + bound};
        const int3 end   = (int3){nx_max - bound, ny_max, nz_max - bound};
        rk3_step_async(m_streams[stream_id], start, end, step_number, dt, m_mesh_info, &d_buffer);
    }

    return AC_SUCCESS;
}
*/

/** Synchronizes the device. If stream_id < 0, synchronizes all streams, otherwise
    synchronizes only stream_id */
AcResult
Device::synchronize(const StreamType stream_type)
{
    cudaSetDevice(m_device_id);
    if (stream_type == STREAM_ALL)
        cudaDeviceSynchronize();
    else
        cudaStreamSynchronize(m_streams[stream_type]);
    return AC_SUCCESS;
}
