static __device__ __forceinline__ AcReal
preprocessed_value(const int3 vertexIdx,  const __restrict__ AcReal* vertex){
return vertex [ IDX(vertexIdx )];
}
static __device__ __forceinline__ AcReal3
preprocessed_gradient(const int3 vertexIdx,  const __restrict__ AcReal* vertex){
return (AcReal3 ){
 derx ( vertexIdx , vertex ), dery ( vertexIdx , vertex ), derz ( vertexIdx , vertex )}
;
}
static __device__ __forceinline__ AcMatrix
preprocessed_hessian(const int3 vertexIdx,  const __restrict__ AcReal* vertex){
AcMatrix hessian ;
hessian . row [ IDX(0 )]= (AcReal3 ){
 derxx ( vertexIdx , vertex ), derxy ( vertexIdx , vertex ), derxz ( vertexIdx , vertex )}
;
hessian . row [ IDX(1 )]= (AcReal3 ){
 hessian . row [ IDX(0 )]. y , deryy ( vertexIdx , vertex ), deryz ( vertexIdx , vertex )}
;
hessian . row [ IDX(2 )]= (AcReal3 ){
 hessian . row [ IDX(0 )]. z , hessian . row [ IDX(1 )]. z , derzz ( vertexIdx , vertex )}
;
return hessian ;
}

typedef struct {
AcReal value;
AcReal3 gradient;
AcMatrix hessian;
} AcRealData;
static __device__ __forceinline__ AcRealData            read_data(const int3 vertexIdx,            AcReal* __restrict__ buf[], const int handle)            {
                AcRealData data;
data.value = preprocessed_value(vertexIdx, buf[handle]);
data.gradient = preprocessed_gradient(vertexIdx, buf[handle]);
data.hessian = preprocessed_hessian(vertexIdx, buf[handle]);
return data;
}
static __device__ __forceinline__ AcReal                    value(const AcRealData& data)                    {
                        return data.value;                    }
static __device__ __forceinline__ AcReal3                    gradient(const AcRealData& data)                    {
                        return data.gradient;                    }
static __device__ __forceinline__ AcMatrix                    hessian(const AcRealData& data)                    {
                        return data.hessian;                    }
        typedef struct {            AcRealData x;            AcRealData y;            AcRealData z;        } AcReal3Data;                static __device__ __forceinline__ AcReal3Data        read_data(const int3 vertexIdx,                  AcReal* __restrict__ buf[], const int3& handle)        {            AcReal3Data data;                    data.x = read_data(vertexIdx, buf, handle.x);            data.y = read_data(vertexIdx, buf, handle.y);            data.z = read_data(vertexIdx, buf, handle.z);                    return data;        }    