;
;
;
;
;
;
;
;
;
;
;
;
;
;
static __device__ __forceinline__  AcReal3
value(const AcReal3Data& uu){
return (AcReal3 ){
 value ( uu . x ), value ( uu . y ), value ( uu . z )}
;
}
static __device__ __forceinline__  AcMatrix
gradients(const AcReal3Data& uu){
return (AcMatrix ){
 gradient ( uu . x ), gradient ( uu . y ), gradient ( uu . z )}
;
}
static __device__ __forceinline__  AcReal
continuity(const AcReal3Data& uu, const AcRealData& lnrho){
return - dot ( value ( uu ), gradient ( lnrho ))- divergence ( uu );
}
static __device__ __forceinline__  AcReal3
momentum(const AcReal3Data& uu, const AcRealData& lnrho, const AcRealData& ss, const AcReal3Data& aa){
const AcMatrix S = stress_tensor ( uu );
const AcReal cs2 = DCONST_REAL(AC_cs2_sound) * exp ( DCONST_REAL(AC_gamma) * value ( ss )/ DCONST_REAL(AC_cp_sound) + (DCONST_REAL(AC_gamma) - 1 )* (value ( lnrho )- LNRHO0 ));
const AcReal3 j = (AcReal ( 1. )/ DCONST_REAL(AC_mu0) )* (gradient_of_divergence ( aa )- laplace_vec ( aa ));
const AcReal3 B = curl ( aa );
const AcReal inv_rho = AcReal ( 1. )/ exp ( value ( lnrho ));
const AcReal3 mom = - mul ( gradients ( uu ), value ( uu ))- cs2 * ((AcReal ( 1. )/ DCONST_REAL(AC_cp_sound) )* gradient ( ss )+ gradient ( lnrho ))+ inv_rho * cross ( j , B )+ DCONST_REAL(AC_nu_visc) * (laplace_vec ( uu )+ AcReal ( 1. / 3. )* gradient_of_divergence ( uu )+ AcReal ( 2. )* mul ( S , gradient ( lnrho )))+ DCONST_REAL(AC_zeta) * gradient_of_divergence ( uu );
return mom ;
}
static __device__ __forceinline__  AcReal3
induction(const AcReal3Data& uu, const AcReal3Data& aa){
const AcReal3 B = curl ( aa );
const AcReal3 grad_div = gradient_of_divergence ( aa );
const AcReal3 lap = laplace_vec ( aa );
const AcReal3 ind = cross ( value ( uu ), B )- DCONST_REAL(AC_eta) * (grad_div - lap );
return ind ;
}
static __device__ __forceinline__  AcReal
lnT(const AcRealData& ss, const AcRealData& lnrho){
const AcReal lnT = LNT0 + DCONST_REAL(AC_gamma) * value ( ss )/ DCONST_REAL(AC_cp_sound) + (DCONST_REAL(AC_gamma) - AcReal ( 1. ))* (value ( lnrho )- LNRHO0 );
return lnT ;
}
static __device__ __forceinline__  AcReal
heat_conduction(const AcRealData& ss, const AcRealData& lnrho){
const AcReal inv_cp_sound = AcReal ( 1. )/ DCONST_REAL(AC_cp_sound) ;
const AcReal3 grad_ln_chi = - gradient ( lnrho );
const AcReal first_term = DCONST_REAL(AC_gamma) * inv_cp_sound * laplace ( ss )+ (DCONST_REAL(AC_gamma) - AcReal ( 1. ))* laplace ( lnrho );
const AcReal3 second_term = DCONST_REAL(AC_gamma) * inv_cp_sound * gradient ( ss )+ (DCONST_REAL(AC_gamma) - AcReal ( 1. ))* gradient ( lnrho );
const AcReal3 third_term = DCONST_REAL(AC_gamma) * (inv_cp_sound * gradient ( ss )+ gradient ( lnrho ))+ grad_ln_chi ;
const AcReal chi = AC_THERMAL_CONDUCTIVITY / (exp ( value ( lnrho ))* DCONST_REAL(AC_cp_sound) );
return DCONST_REAL(AC_cp_sound) * chi * (first_term + dot ( second_term , third_term ));
}
static __device__ __forceinline__  AcReal
heating(const int i , const int j , const int k ){
return 1 ;
}
static __device__ __forceinline__  AcReal
entropy(const AcRealData& ss, const AcReal3Data& uu, const AcRealData& lnrho, const AcReal3Data& aa){
const AcMatrix S = stress_tensor ( uu );
const AcReal inv_pT = AcReal ( 1. )/ (exp ( value ( lnrho ))* exp ( lnT ( ss , lnrho )));
const AcReal3 j = (AcReal ( 1. )/ DCONST_REAL(AC_mu0) )* (gradient_of_divergence ( aa )- laplace_vec ( aa ));
const AcReal RHS = H_CONST - C_CONST + DCONST_REAL(AC_eta) * (AC_mu0 )* dot ( j , j )+ AcReal ( 2. )* exp ( value ( lnrho ))* DCONST_REAL(AC_nu_visc) * contract ( S )+ DCONST_REAL(AC_zeta) * exp ( value ( lnrho ))* divergence ( uu )* divergence ( uu );
return - dot ( value ( uu ), gradient ( ss ))+ inv_pT * RHS + heat_conduction ( ss , lnrho );
}
static __device__ const auto handle_lnrho= VTXBUF_LNRHO ;
static __device__ const auto handle_out_lnrho= VTXBUF_LNRHO ;
static __device__ const auto handle_uu= (int3 ){
 VTXBUF_UUX , VTXBUF_UUY , VTXBUF_UUZ }
;
static __device__ const auto handle_out_uu= (int3 ){
 VTXBUF_UUX , VTXBUF_UUY , VTXBUF_UUZ }
;
static __device__ const auto handle_aa= (int3 ){
 VTXBUF_AX , VTXBUF_AY , VTXBUF_AZ }
;
static __device__ const auto handle_out_aa= (int3 ){
 VTXBUF_AX , VTXBUF_AY , VTXBUF_AZ }
;
static __device__ const auto handle_ss= VTXBUF_ENTROPY ;
static __device__ const auto handle_out_ss= VTXBUF_ENTROPY ;
template <int step_number>  static __global__ void
solve(GEN_KERNEL_PARAM_BOILERPLATE,  AcReal dt ){
GEN_KERNEL_BUILTIN_VARIABLES_BOILERPLATE(); const AcRealData lnrho = READ(handle_lnrho);
AcReal out_lnrho = READ_OUT(handle_out_lnrho);const AcReal3Data uu = READ(handle_uu);
AcReal3 out_uu = READ_OUT(handle_out_uu);const AcReal3Data aa = READ(handle_aa);
AcReal3 out_aa = READ_OUT(handle_out_aa);const AcRealData ss = READ(handle_ss);
AcReal out_ss = READ_OUT(handle_out_ss);out_lnrho = rk3 ( out_lnrho , lnrho , continuity ( uu , lnrho ), dt );
out_aa = rk3 ( out_aa , aa , induction ( uu , aa ), dt );
out_uu = rk3 ( out_uu , uu , momentum ( uu , lnrho , ss , aa ), dt );
out_ss = rk3 ( out_ss , ss , entropy ( ss , uu , lnrho , aa ), dt );
WRITE_OUT(handle_out_lnrho, out_lnrho);
WRITE_OUT(handle_out_uu, out_uu);
WRITE_OUT(handle_out_aa, out_aa);
WRITE_OUT(handle_out_ss, out_ss);
}
