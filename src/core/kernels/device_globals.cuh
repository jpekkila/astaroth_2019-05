/*
    Copyright (C) 2014-2018, Johannes Pekkilae, Miikka Vaeisalae.

    This file is part of Astaroth.

    Astaroth is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Astaroth is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Astaroth.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * \brief info
 *
 * Detailed info.
 *
 */
#pragma once
#include "astaroth.h"

typedef struct {
    AcReal* in[NUM_VTXBUF_HANDLES];
    AcReal* out[NUM_VTXBUF_HANDLES];
} VertexBufferArray;

typedef enum {
    STREAM_PRIMARY,
    STREAM_SECONDARY,
    //STREAM_TERTIARY,
    NUM_STREAM_TYPES
} StreamType;

#define STREAM_ALL (NUM_STREAM_TYPES)

#define DCONST_INT(X) (d_mesh_info.int_params[X])
#define DCONST_REAL(X) (d_mesh_info.real_params[X])

#define DEVICE_VTXBUF_IDX(i, j, k)                                             \
    ((i) + (j)*DCONST_INT(AC_mx) + (k)*DCONST_INT(AC_mxy))
#define DEVICE_1D_COMPDOMAIN_IDX(i, j, k)                                      \
    ((i) + (j)*DCONST_INT(AC_nx) + (k)*DCONST_INT(AC_nxy))

extern VertexBufferArray d_buffer;          // Defined in astaroth.cu
//extern AcReal* d_reduction_scratchpad;      // Defined in device.cu
extern __constant__ AcMeshInfo d_mesh_info; // Defined in device.cu

































