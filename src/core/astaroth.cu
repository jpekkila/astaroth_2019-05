/*
    Copyright (C) 2014-2018, Johannes Pekkilae, Miikka Vaeisalae.

    This file is part of Astaroth.

    Astaroth is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Astaroth is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Astaroth.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * \brief Multi-GPU implementation.
 *
 * Detailed info.
 *
 */
#include "astaroth.h"
#include "device.cuh"

#include "errchk.h"
#include "math_utils.h" // max, min, sum

const char* intparam_names[]      = {AC_FOR_INT_PARAM_TYPES(AC_GEN_STR)};
const char* realparam_names[]     = {AC_FOR_REAL_PARAM_TYPES(AC_GEN_STR)};
const char* vtxbuf_names[]        = {AC_FOR_VTXBUF_HANDLES(AC_GEN_STR)};

static int num_devices = 1;
static Device** devices;

AcMeshInfo mesh_info;
VertexBufferArray d_buffer;     // Unified memory for the GPU VertexBufferArray

AcResult
acInit(const AcMeshInfo& config)
{
    // TODO check that config is valid and all parameters are initialized.
    // Setup the implementations and init everything
    // TODO get valid device ids (select_device function that tracks available
    // devices)

    // Get device count
    // NOTE. ON MULTI-GPU MACHINE COMMENT THIS LINE OUT -->
    cudaGetDeviceCount(&num_devices);
    // <-- ON MULTI-GPU MACHINE COMMENT THIS LINE OUT.
    // TODO One should be able to define the GPU device when running the code.
    // Important for single-gpu work on TIARA gp nodes. They do not have a
    // batch job system.
    if (num_devices < 1) {
        fprintf(stderr, "ERROR: No CUDA devices found.\n");
        return AC_FAILURE;
    }
    devices = (Device**) malloc(sizeof(devices[0]) * num_devices);
    ERRCHK(devices);

    // Create devices
    for (int i = 0; i < num_devices; ++i)
        devices[i] = new Device(i, config);

    const size_t vtxbuf_size_bytes = AC_VTXBUF_SIZE_BYTES(config);
    ///////////// PAD TEST
    //const size_t vtxbuf_size_bytes = AC_VTXBUF_SIZE_BYTES(config) + PAD_LEAD * sizeof(AcReal);
    ///////////// PAD TEST
    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i) {
        ERRCHK_CUDA_ALWAYS(cudaMallocManaged(&d_buffer.in[i], vtxbuf_size_bytes, cudaMemAttachGlobal));
        ERRCHK_CUDA_ALWAYS(cudaMallocManaged(&d_buffer.out[i], vtxbuf_size_bytes, cudaMemAttachGlobal));

        ///////////// PAD TEST
        /*
        d_buffer.in[i] = &d_buffer.in[i][PAD_LEAD]; // TODO NOTE free in acQuit
        d_buffer.out[i] = &d_buffer.out[i][PAD_LEAD]; // TODO NOTE free in acQuit
        ERRCHK(((size_t)&d_buffer.in[i][AC_VTXBUF_IDX(STENCIL_ORDER/2, 0, 0, config)]) % 128
== 0);
        ERRCHK(((size_t)&d_buffer.in[i][AC_VTXBUF_IDX(STENCIL_ORDER/2, 1, 0, config)]) % 128
== 0);
        */
        ///////////// PAD TEST
    }

    mesh_info = config;

    return AC_SUCCESS;
}

AcResult
acQuit(void)
{
    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i) {
        ERRCHK_CUDA_ALWAYS(cudaFree(d_buffer.in[i]));
        ERRCHK_CUDA_ALWAYS(cudaFree(d_buffer.out[i]));

        ///////////// PAD TEST
        /*
        ERRCHK_CUDA_ALWAYS(cudaFree(&d_buffer.in[i][-PAD_LEAD])); // TODO don't do this at home
        ERRCHK_CUDA_ALWAYS(cudaFree(&d_buffer.out[i][-PAD_LEAD])); // TODO don't do this at home
        */
        ///////////// PAD TEST
    }

    for (int i = 0; i < num_devices; ++i)
        delete devices[i];

    free(devices);
    return AC_SUCCESS;
}

AcResult
acLoadWithOffset(const AcMesh& host_mesh, const int3& start, const int num_vertices)
{
    const size_t idx = AC_VTXBUF_IDX(start.x, start.y, start.z, host_mesh.info);
    ERRCHK(idx + num_vertices <= AC_VTXBUF_SIZE(host_mesh.info));

    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i)
        ERRCHK_CUDA(cudaMemcpy(&d_buffer.in[i][idx], &host_mesh.vertex_buffer[i][idx], sizeof(host_mesh.vertex_buffer[i][0]) * num_vertices, cudaMemcpyHostToDevice));

    return AC_SUCCESS;
}

AcResult
acStoreWithOffset(const int3& start, const int num_vertices, AcMesh* host_mesh)
{
    const size_t idx = AC_VTXBUF_IDX(start.x, start.y, start.z, host_mesh->info);
    ERRCHK(idx + num_vertices <= AC_VTXBUF_SIZE(host_mesh->info));

    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i)
        ERRCHK_CUDA(cudaMemcpy(&host_mesh->vertex_buffer[i][idx], &d_buffer.in[i][idx], sizeof(host_mesh->vertex_buffer[i][0]) * num_vertices, cudaMemcpyDeviceToHost));

    return AC_SUCCESS;
}

AcResult
acLoad(const AcMesh& host_mesh)
{
    return acLoadWithOffset(host_mesh, (int3){0, 0, 0}, AC_VTXBUF_SIZE(host_mesh.info));
}

AcResult
acStore(AcMesh* host_mesh)
{
    return acStoreWithOffset((int3){0, 0, 0}, AC_VTXBUF_SIZE(host_mesh->info), host_mesh);
}

static AcResult
acSwapBuffers(void)
{
    //acSynchronize();
    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i) {
        AcReal* tmp     = d_buffer.in[i];
        d_buffer.in[i]  = d_buffer.out[i];
        d_buffer.out[i] = tmp;
    }
    return AC_SUCCESS;
}

AcResult
acIntegrate(const AcReal& dt)
{
    for (int i = 0; i < 3; ++i) {
        acBoundcondStep();
        acIntegrateStep(i, dt);
        acSwapBuffers();
    }
    return AC_SUCCESS;
}

AcResult
acIntegrateStep(const int& isubstep, const AcReal& dt)
{
    for (int i = 0; i < num_devices; ++i) {
        devices[i]->rk3_step(STREAM_PRIMARY, isubstep, dt);
        //devices[i]->rk3_step_inner(STREAM_SECONDARY, isubstep, dt);
        //devices[i]->rk3_step_outer(STREAM_PRIMARY, isubstep, dt);
    }

    return AC_SUCCESS;
}

#include "kernels/boundconds.cuh"

AcResult
acBoundcondStepPeriodic(void)
{

    cudaSetDevice(0);
    for (int device_id = 0; device_id < num_devices; ++device_id) {
        //cudaSetDevice(device_id);
    #if BOUNDCONDS_OPTIMIZE

    dim3 best_dims(0, 0, 0);
    float best_time = INFINITY;

    for (int z = 1; z <= MAX_TB_DIM; ++z) {
    for (int y = 1; y <= MAX_TB_DIM; ++y) {
    for (int x = 1; x <= MAX_TB_DIM; ++x) {
        const int depth = (int)ceil(mesh_info.int_params[AC_mz]/(float)num_devices);
        const int3 start = (int3){0, 0, device_id * depth};
        const int3 end = (int3){mesh_info.int_params[AC_mx],
                                mesh_info.int_params[AC_my],
                                min((device_id+1) * depth, mesh_info.int_params[AC_mz])};
        if (x > end.x - start.x ||
            y > end.y - start.y ||
            z > end.z - start.z)
            break;
        if (x*y*z > MAX_THREADS_PER_BLOCK)
            break;

        if (x*y*z*REGISTERS_PER_THREAD > MAX_REGISTERS_PER_BLOCK)
            break;

        if (((x*y*z) % WARP_SIZE) != 0)
            continue;

    const dim3 tpb(x, y, z);

    // Warmup and early exit if fail
    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i) {
        periodic_boundconds(0, tpb, start, end, d_buffer.in[i]);
    }
    cudaDeviceSynchronize();
    if (cudaGetLastError() != cudaSuccess) // resets the error if any
        continue;

    printf("(%d, %d, %d)\n", x, y, z);

    cudaEvent_t tstart, tstop;
    cudaEventCreate(&tstart);
    cudaEventCreate(&tstop);

    cudaEventRecord(tstart);
    for (int i = 0; i < NUM_ITERATIONS; ++i) {
        for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i) {
            periodic_boundconds(0, tpb, start, end, d_buffer.in[i]);
        }
    }
    cudaEventRecord(tstop);
    cudaEventSynchronize(tstop);
    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, tstart, tstop);


    //ERRCHK_CUDA_KERNEL_ALWAYS();
    if (milliseconds < best_time) {
        best_time = milliseconds;
        best_dims = tpb;
    }
    }}}

    printf("Boundconds best dims (%d, %d, %d) %f ms\n", best_dims.x, best_dims.y, best_dims.z, double(best_time) / NUM_ITERATIONS);

    exit(0);
    #else


        const int depth = (int)ceil(mesh_info.int_params[AC_mz]/(float)num_devices);

        const int3 start = (int3){0, 0, device_id * depth};
        const int3 end = (int3){mesh_info.int_params[AC_mx],
                                mesh_info.int_params[AC_my],
                                min((device_id+1) * depth, mesh_info.int_params[AC_mz])};

        const dim3 tpb(8,2,8);

        // TODO uses the default stream currently
        for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i) {
            periodic_boundconds(0, tpb, start, end, d_buffer.in[i]);
        }
    #endif
    }

    return AC_SUCCESS;
}

AcResult
acBoundcondStep(void)
{
    acBoundcondStepPeriodic();

    #if LTEMPERATURE
       const dim3 tpb(8,2,8);
       const int3 bottom_start = (int3){0, 0, 0};
       const int3 bottom_end = (int3){0, 0, mesh_info.int_params[AC_nz_min]};

        const int3 top_start = (int3) {mesh_info.int_params[AC_nx_max], mesh_info.int_params[AC_ny_max], mesh_info.int_params[AC_nz_max]};
        const int3 top_end = (int3){top_start.x + STENCIL_ORDER/2, top_start.y + STENCIL_ORDER/2, top_start.z + STENCIL_ORDER/2};

        antisymmetric_boundconds(0, tpb, bottom_start, bottom_end, Z_AXIS, 0, d_buffer.in[VTXBUF_UUX]);
        antisymmetric_boundconds(0, tpb, bottom_start, bottom_end, Z_AXIS, 0, d_buffer.in[VTXBUF_UUY]);
        antisymmetric_boundconds(0, tpb, bottom_start, bottom_end, Z_AXIS, 0, d_buffer.in[VTXBUF_UUZ]);
        antisymmetric_boundconds(0, tpb, top_start, top_end, Z_AXIS, 0, d_buffer.in[VTXBUF_UUX]);
        antisymmetric_boundconds(0, tpb, top_start, top_end, Z_AXIS, 0, d_buffer.in[VTXBUF_UUY]);
        antisymmetric_boundconds(0, tpb, top_start, top_end, Z_AXIS, 0, d_buffer.in[VTXBUF_UUZ]);

        antisymmetric_boundconds(0, tpb, bottom_start, bottom_end, Z_AXIS, -1, d_buffer.in[VTXBUF_LNRHO]);
        antisymmetric_boundconds(0, tpb, top_start, top_end, Z_AXIS, -1, d_buffer.in[VTXBUF_LNRHO]);

        antisymmetric_boundconds(0, tpb, bottom_start, bottom_end, Z_AXIS, 100, d_buffer.in[VTXBUF_TEMPERATURE]);
        antisymmetric_boundconds(0, tpb, top_start, top_end, Z_AXIS, 10, d_buffer.in[VTXBUF_TEMPERATURE]);

    #endif
    return AC_SUCCESS;
}

static AcReal
simple_final_reduce_scal(const ReductionType& rtype, const AcReal* results,
                         const int& n)
{
    AcReal res = results[0];
    for (int i = 1; i < n; ++i) {
        if (rtype == RTYPE_MAX)
            res = max(res, results[i]);
        else if (rtype == RTYPE_MIN)
            res = min(res, results[i]);
        else if (rtype == RTYPE_RMS || rtype == RTYPE_RMS_EXP)
            res = sum(res, results[i]);
        else
            ERROR("Invalid rtype");
    }

    if (rtype == RTYPE_RMS || rtype == RTYPE_RMS_EXP)
        res = sqrt(res);

    return res;
}

AcReal
acReduceScal(const ReductionType& rtype,
             const VertexBufferHandle& vtxbuffer_handle)
{
    AcReal results[num_devices];
    for (int i = 0; i < num_devices; ++i)
        results[i] = devices[i]->reduce_scal(STREAM_PRIMARY, rtype, vtxbuffer_handle);

    return simple_final_reduce_scal(rtype, results, num_devices);
}

AcReal
acReduceVec(const ReductionType& rtype, const VertexBufferHandle& a,
            const VertexBufferHandle& b, const VertexBufferHandle& c)
{
    AcReal results[num_devices];
    for (int i = 0; i < num_devices; ++i)
        results[i] = devices[i]->reduce_vec(STREAM_PRIMARY, rtype, a, b, c);

    return simple_final_reduce_scal(rtype, results, num_devices);
}

AcResult
acSynchronize(void)
{
    for (int i = 0; i < num_devices; ++i)
        devices[i]->synchronize(STREAM_ALL);
    return AC_SUCCESS;
}
