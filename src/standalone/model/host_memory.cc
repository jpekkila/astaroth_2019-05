/*
    Copyright (C) 2014-2018, Johannes Pekkilae, Miikka Vaeisalae.

    This file is part of Astaroth.

    Astaroth is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Astaroth is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Astaroth.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * \brief Brief info.
 *
 * Detailed info.
 *
 */
#include "host_memory.h"

#include <math.h>

#include "core/errchk.h"

const char* init_type_names[] = {AC_FOR_INIT_TYPES(AC_GEN_STR)};

#define XORIG (AcReal(.5) * mesh->info.int_params[AC_nx] * mesh->info.real_params[AC_dsx])
#define YORIG (AcReal(.5) * mesh->info.int_params[AC_ny] * mesh->info.real_params[AC_dsy])
#define ZORIG (AcReal(.5) * mesh->info.int_params[AC_nz] * mesh->info.real_params[AC_dsz])

/*
#include <stdint.h>
static uint64_t ac_rand_next = 1;

static int32_t
ac_rand(void)
{
	ac_rand_next = ac_rand_next * 1103515245 + 12345;
	return (uint32_t)(ac_rand_next/65536) % 32768;
}

static void
ac_srand(const uint32_t seed)
{
	ac_rand_next = seed;	
}
*/

AcMesh*
acmesh_create(const AcMeshInfo& mesh_info)
{
    AcMesh* mesh = (AcMesh*)malloc(sizeof(*mesh));
    mesh->info   = mesh_info;

    const size_t bytes = AC_VTXBUF_SIZE_BYTES(mesh->info);
    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i) {
        mesh->vertex_buffer[VertexBufferHandle(i)] = (AcReal*)malloc(bytes);
        ERRCHK(mesh->vertex_buffer[VertexBufferHandle(i)] != NULL);
    }

    return mesh;
}

static void
vertex_buffer_set(const VertexBufferHandle& key, const AcReal& val,
                  AcMesh* mesh)
{
    const int n = AC_VTXBUF_SIZE(mesh->info);
    for (int i = 0; i < n; ++i)
        mesh->vertex_buffer[key][i] = val;
}


/** Inits all fields to 1. Setting the mesh to zero is problematic because some fields are supposed
    to be > 0 and the results would vary widely, which leads to loss of precision in the
    computations */
void
acmesh_clear(AcMesh* mesh)
{
    for (int w = 0; w < NUM_VTXBUF_HANDLES; ++w)
        vertex_buffer_set(VertexBufferHandle(w), 1, mesh); // Init all fields to 1 by default.
}

static AcReal
randr(void)
{
    return AcReal(rand()) / AcReal(RAND_MAX);
}

void
gaussian_radial_explosion(AcMesh* mesh)
{
    AcReal* uu_x = mesh->vertex_buffer[VTXBUF_UUX];
    AcReal* uu_y = mesh->vertex_buffer[VTXBUF_UUY];
    AcReal* uu_z = mesh->vertex_buffer[VTXBUF_UUZ];

    const int mx = mesh->info.int_params[AC_mx];
    const int my = mesh->info.int_params[AC_my];

    const int nx_min = mesh->info.int_params[AC_nx_min];
    const int nx_max = mesh->info.int_params[AC_nx_max];
    const int ny_min = mesh->info.int_params[AC_ny_min];
    const int ny_max = mesh->info.int_params[AC_ny_max];
    const int nz_min = mesh->info.int_params[AC_nz_min];
    const int nz_max = mesh->info.int_params[AC_nz_max];

    const double DX    = mesh->info.real_params[AC_dsx];
    const double DY    = mesh->info.real_params[AC_dsy];
    const double DZ    = mesh->info.real_params[AC_dsz];

    const double xorig = double(XORIG) - 0.000001; 
    const double yorig = double(YORIG) - 0.000001;
    const double zorig = double(ZORIG) - 0.000001;

    const double INIT_LOC_UU_X = 0.0;
    const double INIT_LOC_UU_Y = 0.0;
    const double INIT_LOC_UU_Z = 0.0;

    const double AMPL_UU    = mesh->info.real_params[AC_ampl_uu];
    const double UU_SHELL_R = 0.8;
    const double WIDTH_UU   = 0.2;

    // Outward explosion with gaussian initial velocity profile.
    int idx;
    double xx, yy, zz, rr2, rr, theta = 0.0, phi = 0.0;
    double uu_radial;

    // double theta_old = 0.0;

    for (int k = nz_min; k < nz_max; k++) {
        for (int j = ny_min; j < ny_max; j++) {
            for (int i = nx_min; i < nx_max; i++) {
                // Calculate the value of velocity in a particular radius.
                idx = i + j * mx + k * mx * my;
                // Determine the coordinates
                xx = DX * (i - nx_min) - xorig;
                xx = xx - INIT_LOC_UU_X;

                yy = DY * (j - ny_min) - yorig;
                yy = yy - INIT_LOC_UU_Y;

                zz = DZ * (k - nz_min) - zorig;
                zz = zz - INIT_LOC_UU_Z;

                rr2 = pow(xx, 2.0) + pow(yy, 2.0) + pow(zz, 2.0);
                rr  = sqrt(rr2);

                // Origin is different!
                double xx_abs, yy_abs, zz_abs;
                if (rr > 0.0) {
                    // theta range [0, PI]
                    if (zz >= 0.0) {
                        theta = acos(zz / rr);
                        if (theta > M_PI / 2.0 || theta < 0.0) {
                            printf("Explosion THETA WRONG: zz = %.3f, rr = "
                                   "%.3f, theta = %.3e/PI, M_PI = %.3e\n",
                                   zz, rr, theta / M_PI, M_PI);
                        }
                    }
                    else {
                        zz_abs = -zz; // Needs a posite value for acos
                        theta  = M_PI - acos(zz_abs / rr);
                        if (theta < M_PI / 2.0 || theta > 2 * M_PI) {
                            printf("Explosion THETA WRONG: zz = %.3f, rr = "
                                   "%.3f, theta = %.3e/PI, M_PI = %.3e\n",
                                   zz, rr, theta / M_PI, M_PI);
                        }
                    }

                    // phi range [0, 2*PI]i
                    if (xx != 0.0) {
                        if (xx < 0.0 && yy >= 0.0) {
                            //-+
                            xx_abs = -xx; // Needs a posite value for atan
                            phi    = M_PI - atan(yy / xx_abs);
                            if (phi < (M_PI / 2.0) || phi > M_PI) {
                                printf("Explosion PHI WRONG -+: xx = %.3f, yy "
                                       "= %.3f, phi = %.3e/PI, M_PI = %.3e\n",
                                       xx, yy, phi / M_PI, M_PI);
                            }
                        }
                        else if (xx > 0.0 && yy < 0.0) {
                            //+-
                            yy_abs = -yy;
                            phi    = 2.0 * M_PI - atan(yy_abs / xx);
                            if (phi < (3.0 * M_PI) / 2.0 ||
                                phi > (2.0 * M_PI + 1e-6)) {
                                printf("Explosion PHI WRONG +-: xx = %.3f, yy "
                                       "= %.3f, phi = %.3e/PI, M_PI = %.3e\n",
                                       xx, yy, phi / M_PI, M_PI);
                            }
                        }
                        else if (xx < 0.0 && yy < 0.0) {
                            //--
                            yy_abs = -yy;
                            xx_abs = -xx;
                            phi    = M_PI + atan(yy_abs / xx_abs);
                            if (phi < M_PI ||
                                phi > ((3.0 * M_PI) / 2.0 + 1e-6)) {
                                printf("Explosion PHI WRONG --: xx = %.3f, yy "
                                       "= %.3f, xx_abs = %.3f, yy_abs = %.3f, "
                                       "phi = %.3e, (3.0*M_PI)/2.0 = %.3e\n",
                                       xx, yy, xx_abs, yy_abs, phi,
                                       (3.0 * M_PI) / 2.0);
                            }
                        }
                        else {
                            //++
                            phi = atan(yy / xx);
                            if (phi < 0 || phi > M_PI / 2.0) {
                                printf(
                                    "Explosion PHI WRONG --: xx = %.3f, yy = "
                                    "%.3f, phi = %.3e, (3.0*M_PI)/2.0 = %.3e\n",
                                    xx, yy, phi, (3.0 * M_PI) / 2.0);
                            }
                        }
                    }
                    else { // To avoid div by zero with atan
                        if (yy > 0.0) {
                            phi = M_PI / 2.0;
                        }
                        else if (yy < 0.0) {
                            phi = (3.0 * M_PI) / 2.0;
                        }
                        else {
                            phi = 0.0;
                        }
                    }

                    // Set zero for explicit safekeeping
                    if (xx == 0.0 && yy == 0.0) {
                        phi = 0.0;
                    }

                    // Gaussian velocity
                    // uu_radial = AMPL_UU*exp( -rr2 / (2.0*pow(WIDTH_UU, 2.0))
                    // ); New distribution, where that gaussion wave is not in
                    // the exact centre coordinates uu_radial = AMPL_UU*exp(
                    // -pow((rr - 4.0*WIDTH_UU),2.0) / (2.0*pow(WIDTH_UU, 2.0))
                    // ); //TODO: Parametrize the peak location.
                    uu_radial = AMPL_UU * exp(-pow((rr - UU_SHELL_R), 2.0) /
                                              (2.0 * pow(WIDTH_UU, 2.0)));
                }
                else {
                    uu_radial = 0.0; // TODO: There will be a discontinuity in
                                     // the origin... Should the shape of the
                                     // distribution be different?
                }

                // Determine the carthesian velocity components and lnrho
                uu_x[idx] = AcReal(uu_radial * sin(theta) * cos(phi));
                uu_y[idx] = AcReal(uu_radial * sin(theta) * sin(phi));
                uu_z[idx] = AcReal(uu_radial * cos(theta));

                // Temporary diagnosticv output (TODO: Remove after not needed)
                // if (theta > theta_old) {
                // if (theta > M_PI || theta < 0.0 || phi < 0.0 || phi > 2*M_PI)
                // {
                /*	printf("Explosion: xx = %.3f, yy = %.3f, zz = %.3f, rr =
                   %.3f, phi = %.3e/PI, theta = %.3e/PI\n, M_PI = %.3e", xx, yy,
                   zz, rr, phi/M_PI, theta/M_PI, M_PI); printf(" uu_radial =
                   %.3e, uu_x[%i] = %.3e, uu_y[%i] = %.3e, uu_z[%i] = %.3e \n",
                                uu_radial, idx, uu_x[idx], idx, uu_y[idx], idx,
                   uu_z[idx]); theta_old = theta;
                */
            }
        }
    }
}

void
acmesh_init_to(const InitType& init_type, AcMesh* mesh)
{
    srand(123456789);


    const int n = AC_VTXBUF_SIZE(mesh->info);

    const int mx = mesh->info.int_params[AC_mx];
    const int my = mesh->info.int_params[AC_my];
    const int mz = mesh->info.int_params[AC_mz];

    const int nx_min = mesh->info.int_params[AC_nx_min];
    const int nx_max = mesh->info.int_params[AC_nx_max];
    const int ny_min = mesh->info.int_params[AC_ny_min];
    const int ny_max = mesh->info.int_params[AC_ny_max];
    const int nz_min = mesh->info.int_params[AC_nz_min];
    const int nz_max = mesh->info.int_params[AC_nz_max];

    switch (init_type) {
    case INIT_TYPE_RANDOM: {
        acmesh_clear(mesh);
        const AcReal range = AcReal(0.01);
        for (int w = 0; w < NUM_VTXBUF_HANDLES; ++w)
            for (int i = 0; i < n; ++i)
                mesh->vertex_buffer[w][i] = 2 * range * randr() - range;

        break;
    }
    case INIT_TYPE_GAUSSIAN_RADIAL_EXPL:
        acmesh_clear(mesh);
        //acmesh_init_to(INIT_TYPE_RANDOM, mesh);
        gaussian_radial_explosion(mesh);

        break;
    case INIT_TYPE_XWAVE:
        acmesh_clear(mesh);
        acmesh_init_to(INIT_TYPE_RANDOM, mesh);
        for (int k = 0; k < mz; k++) {
            for (int j = 0; j < my; j++) {
                for (int i = 0; i < mx; i++) {
                    int idx = i + j * mx + k * mx * my;
                    mesh->vertex_buffer[VTXBUF_UUX][idx] = 2*AcReal(sin(j * AcReal(M_PI) / mx)) - 1;
                }
            }
        }
        break;
    case INIT_TYPE_ABC_FLOW: {
        acmesh_clear(mesh);
        acmesh_init_to(INIT_TYPE_RANDOM, mesh);
        for (int k = nz_min; k < nz_max; k++) {
            for (int j = ny_min; j < ny_max; j++) {
                for (int i = nx_min; i < nx_max; i++) {
                    const int idx = i + j * mx + k * mx * my;

                    /*
                    const double xx = double(
                        mesh->info.real_params[AC_dsx] *
                            (i - mesh->info.int_params[AC_nx_min]) -
                        XORIG + AcReal(.5) * mesh->info.real_params[AC_dsx]);
                    const double yy = double(
                        mesh->info.real_params[AC_dsy] *
                            (j - mesh->info.int_params[AC_ny_min]) -
                        YORIG + AcReal(.5) * mesh->info.real_params[AC_dsy]);
                    const double zz = double(
                        mesh->info.real_params[AC_dsz] *
                            (k - mesh->info.int_params[AC_nz_min]) -
                        ZORIG + AcReal(.5) * mesh->info.real_params[AC_dsz]);
                    */

                    const AcReal xx = (i - nx_min) * mesh->info.real_params[AC_dsx] - XORIG;
                    const AcReal yy = (j - ny_min) * mesh->info.real_params[AC_dsy] - YORIG;
                    const AcReal zz = (k - nz_min) * mesh->info.real_params[AC_dsz] - ZORIG;

                    const AcReal ampl_uu = 0.5;
                    const AcReal ABC_A   = 1.;
                    const AcReal ABC_B   = 1.;
                    const AcReal ABC_C   = 1.;
                    const AcReal kx_uu   = 8.;
                    const AcReal ky_uu   = 8.;
                    const AcReal kz_uu   = 8.;

                    mesh->vertex_buffer[VTXBUF_UUX][idx] = ampl_uu * (ABC_A * (AcReal)sin(kz_uu * zz) + ABC_C * (AcReal)cos(ky_uu * yy));
                    mesh->vertex_buffer[VTXBUF_UUY][idx] = ampl_uu * (ABC_B * (AcReal)sin(kx_uu * xx) + ABC_A * (AcReal)cos(kz_uu * zz));
                    mesh->vertex_buffer[VTXBUF_UUZ][idx] = ampl_uu * (ABC_C * (AcReal)sin(ky_uu * yy) + ABC_B * (AcReal)cos(kx_uu * xx));
                }
            }
        }
        break;
    }
    /*
    case INIT_TYPE_RAYLEIGH_BENARD: {
        acmesh_init_to(INIT_TYPE_RANDOM, mesh);
        #if LTEMPERATURE
        vertex_buffer_set(VTXBUF_LNRHO, 1, mesh);
        const AcReal range = AcReal(0.9);
        for (int k = nz_min; k < nz_max; k++) {
            for (int j = ny_min; j < ny_max; j++) {
                for (int i = nx_min; i < nx_max; i++) {
                    const int idx = i + j * mx + k * mx * my;
                    mesh->vertex_buffer[VTXBUF_TEMPERATURE][idx] = (range * (k - nz_min)) / mesh->info.int_params[AC_nz] + 0.1;
                }
            }
        }
        #else
        WARNING("INIT_TYPE_RAYLEIGH_BERNARD called even though VTXBUF_TEMPERATURE is not used");
        #endif
        break;
    }
    */
    default:
        ERROR("Unknown init_type");
    }

    AcReal max_val = AcReal(-1e-32);
    AcReal min_val = AcReal(1e32);
    // Normalize the grid
    for (int w = 0; w < NUM_VTXBUF_HANDLES; ++w) {
        for (int i = 0; i < n; ++i) {
            if (mesh->vertex_buffer[w][i] < min_val)
                min_val = mesh->vertex_buffer[w][i];
            if (mesh->vertex_buffer[w][i] > max_val)
                max_val = mesh->vertex_buffer[w][i];
        }
    }
    printf("MAX: %f MIN %f\n", double(max_val), double(min_val));
    /*
    const AcReal inv_range = AcReal(1.) / fabs(max_val - min_val);
    for (int w = 0; w < NUM_VTXBUF_HANDLES; ++w) {
        for (int i = 0; i < n; ++i) {
            mesh->vertex_buffer[w][i] = 2*inv_range*(mesh->vertex_buffer[w][i] - min_val) - 1;
        }
    }
    */
}

void
acmesh_destroy(AcMesh* mesh)
{
    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i)
        free(mesh->vertex_buffer[VertexBufferHandle(i)]);

    free(mesh);
}


ModelMesh*
modelmesh_create(const AcMeshInfo& mesh_info)
{
    ModelMesh* mesh = (ModelMesh*)malloc(sizeof(*mesh));
    mesh->info   = mesh_info;

    const size_t bytes = AC_VTXBUF_SIZE(mesh->info) * sizeof(mesh->vertex_buffer[0][0]);
    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i) {
        mesh->vertex_buffer[VertexBufferHandle(i)] = (ModelScalar*)malloc(bytes);
        ERRCHK(mesh->vertex_buffer[VertexBufferHandle(i)] != NULL);
    }

    return mesh;
}

void
modelmesh_destroy(ModelMesh* mesh)
{
    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i)
        free(mesh->vertex_buffer[VertexBufferHandle(i)]);

    free(mesh);
}

#include <string.h> // memcpy
void
acmesh_to_modelmesh(const AcMesh& acmesh, ModelMesh* modelmesh)
{
    ERRCHK(sizeof(acmesh.info) == sizeof(modelmesh->info));
    memcpy(&modelmesh->info, &acmesh.info, sizeof(acmesh.info));

    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i)
        for (size_t j = 0; j < AC_VTXBUF_SIZE(acmesh.info); ++j)
            modelmesh->vertex_buffer[i][j] = (ModelScalar)acmesh.vertex_buffer[i][j];
}

void
modelmesh_to_acmesh(const ModelMesh& modelmesh, AcMesh* acmesh)
{
    ERRCHK(sizeof(acmesh->info) == sizeof(modelmesh.info));
    memcpy(&acmesh->info, &modelmesh.info, sizeof(modelmesh.info));

    for (int i = 0; i < NUM_VTXBUF_HANDLES; ++i)
        for (size_t j = 0; j < AC_VTXBUF_SIZE(modelmesh.info); ++j)
            acmesh->vertex_buffer[i][j] = (AcReal)modelmesh.vertex_buffer[i][j];
}
