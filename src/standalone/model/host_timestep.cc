/*
    Copyright (C) 2014-2018, Johannes Pekkilae, Miikka Vaeisalae.

    This file is part of Astaroth.

    Astaroth is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Astaroth is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Astaroth.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file
 * \brief Brief info.
 *
 * Detailed info.
 *
 */
#include "host_timestep.h"

#include "core/math_utils.h"

static AcReal timescale = AcReal(1.0);
#include <stdio.h>
AcReal
host_timestep(void) //const AcReal& umax, const AcMeshInfo& mesh_info)
{
    /*
    const long double cdt      = mesh_info.real_params[AC_cdt];
    const long double cdtv     = mesh_info.real_params[AC_cdtv];
    // const long double cdts     = mesh_info.real_params[AC_cdts];
    const long double cs2_sound = mesh_info.real_params[AC_cs2_sound];
    const long double nu_visc  = mesh_info.real_params[AC_nu_visc];
    const long double eta      = mesh_info.real_params[AC_eta];
    const long double chi      = mesh_info.real_params[AC_chi];
    const long double gamma    = mesh_info.real_params[AC_gamma];
    const long double dsmin    = mesh_info.real_params[AC_dsmin];

    // Old ones from legacy Astaroth
    //const long double uu_dt   = cdt * (dsmin / (umax + cs_sound));
    //const long double visc_dt = cdtv * dsmin * dsmin / nu_visc;

    // New, closer to the actual Courant timestep
    // See Pencil Code user manual p. 38 (timestep section)
    //const long double uu_dt   = cdt * dsmin / (fabsl(umax) + sqrtl(cs2_sound + 0.0l));
    //const long double visc_dt = cdtv * dsmin * dsmin / max(max(nu_visc, eta), max(gamma, chi)) + 1; // TODO NOTE: comment the +1 out to get scientifically accurate results
    //const long double dt = min(uu_dt, visc_dt);

    const long double rho_max = 1; // TODO
    const long double ss_max = 1; // TODO
    // NOTE!! rho_max and ss_max should always be positive!

    const long double alfven_speed = 0; // TODO
    const long double D_max = max(max(nu_visc, gamma * chi), max(eta, rho_max * 1)); // TODO
    const long double cv_sound = mesh_info.real_params[AC_cv_sound];

    const long double advective_ts = cdt * dsmin / (umax + sqrtl(cs2_sound + alfven_speed*alfven_speed));
    const long double diffusive_ts = cdtv * dsmin * dsmin / D_max + 1;
    const long double entropy_ts = ss_max / cv_sound;

    const long double dt = min(advective_ts, min(diffusive_ts, entropy_ts));
    return AcReal(timescale) * AcReal(dt);
    */
    // Disabled variable timestep
    return AcReal(1e-4);
}

void
set_timescale(const AcReal scale)
{
    timescale = scale;
}
