#!/bin/bash
cd `dirname $0` # Only operate in the same directory with this script

./build_acc.sh

mkdir -p testbin
./compile.sh samples/sample_stencil_process.sps
./compile.sh samples/sample_stencil_assembly.sas

mv stencil_process.cuh testbin/
mv stencil_assembly.cuh testbin/

printf "
#include <stdio.h>
#include <stdlib.h>
#include \"%s\"
#include \"%s\"
#include \"%s\"
int main(void) { printf(\"Success\\\n\"); return EXIT_SUCCESS; }
" common_header.h stencil_assembly.cuh stencil_process.cuh >testbin/test.cu

cd testbin
nvcc -std=c++11 test.cu -I ../samples -o test && ./test
