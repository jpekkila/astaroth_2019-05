#SRUN_COMMAND="srun -J astaroth -p gpu --nodes=1 --gres=gpu:p100:1 --ntasks-per-node=1 --cpus-per-task=14 -t 02:59:00 --mem=48000"
SRUN_COMMAND="srun -J astaroth -p gputest --nodes=1 --gres=gpu:k80:1 --ntasks-per-node=1 --cpus-per-task=12 -t 00:10:00 --mem=48000"

module load cmake cuda

cd build
../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=OFF -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=ON .. && make -j && ${SRUN_COMMAND} ./ac_run -b
wait
../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=OFF -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=ON .. && make -j && ${SRUN_COMMAND} ./ac_run -b
wait
mv float_rk3substep_runningtimes.out float_rk3substep_runningtimes_2nd_ord.out
mv double_rk3substep_runningtimes.out double_rk3substep_runningtimes_2nd_ord.out
