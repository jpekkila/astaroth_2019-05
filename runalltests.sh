#!/bin/bash
SRUN_COMMAND="srun -J astaroth -p gpu --nodes=1 --gres=gpu:p100:1 --ntasks-per-node=1 --cpus-per-task=14 -t 02:59:00 --mem=48000"
USE_DOUBLE="OFF"

module load cmake cuda
source ./sourceme.sh
cd build

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=ON -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} ./ac_run -s

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=ON .. && make -j && ${SRUN_COMMAND} ./ac_run -b

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} ./ac_run -b

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} nvprof --metrics stall_constant_memory_dependency,stall_exec_dependency,stall_inst_fetch,stall_memory_dependency,stall_memory_throttle,stall_not_selected,stall_other,stall_pipe_busy,stall_sync,stall_texture --log-file 256_sp_detailed_hw_stalls.out ./ac_run -s

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} nvprof --metrics dram_read_throughput,dram_write_throughput,tex_cache_throughput,l2_read_throughput,l2_write_throughput,tex_cache_hit_rate,l2_tex_hit_rate,local_hit_rate,flop_sp_efficiency,flop_dp_efficiency,issue_slot_utilization,ldst_fu_utilization,dram_utilization,l2_utilization,shared_utilization,achieved_occupancy,sm_efficiency --log-file 256_sp_detailed_hw_utilization.out ./ac_run -s

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} nvprof --kernels solve --analysis-metrics -o  rk3substep_sp_analysis.nvprof -f ./ac_run -s

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} ./ac_run -t

wait
USE_DOUBLE="ON"

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=ON -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} ./ac_run -s

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=ON .. && make -j && ${SRUN_COMMAND} ./ac_run -b

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} ./ac_run -b

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} nvprof --metrics stall_constant_memory_dependency,stall_exec_dependency,stall_inst_fetch,stall_memory_dependency,stall_memory_throttle,stall_not_selected,stall_other,stall_pipe_busy,stall_sync,stall_texture --log-file 256_dp_detailed_hw_stalls.out ./ac_run -s

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} nvprof --metrics dram_read_throughput,dram_write_throughput,tex_cache_throughput,l2_read_throughput,l2_write_throughput,tex_cache_hit_rate,l2_tex_hit_rate,local_hit_rate,flop_sp_efficiency,flop_dp_efficiency,issue_slot_utilization,ldst_fu_utilization,dram_utilization,l2_utilization,shared_utilization,achieved_occupancy,sm_efficiency --log-file 256_dp_detailed_hw_utilization.out ./ac_run -s

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && nvprof --kernels solve --analysis-metrics -o  rk3substep_dp_analysis.nvprof -f ${SRUN_COMMAND} ./ac_run -s

wait

../scripts/compile_acc.sh&& cmake -DDOUBLE_PRECISION=${USE_DOUBLE} -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} ./ac_run -t

