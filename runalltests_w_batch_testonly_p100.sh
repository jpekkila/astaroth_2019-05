#!/bin/bash
SRUN_COMMAND="" #"srun -J astaroth -p gpu --nodes=1 --gres=gpu:p100:1 --ntasks-per-node=1 --cpus-per-task=14 -t 09:00:00 --mem=48000"

# set gputest-> gpu, k80 -> p100, time to appropriate, cpus-per-task 12 -> 14

module load cmake cuda
source ./sourceme.sh
cd build


cmake -DDOUBLE_PRECISION=OFF -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} ./ac_run -t
wait
cmake -DDOUBLE_PRECISION=ON -DRK3_OPTIMIZE=OFF -DGEN_BENCHMARK_RK3=OFF .. && make -j && ${SRUN_COMMAND} ./ac_run -t
